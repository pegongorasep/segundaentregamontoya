package com.montoya.segundaentregamontoya.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.montoya.segundaentregamontoya.R;
import com.montoya.segundaentregamontoya.models.ToDoItem;
import java.util.List;

/**
 * Adapter de nuestra recyclerview de TODOs para poder configurar cada item
 */
public class ToDoItemsRecyclerViewAdapter extends RecyclerView.Adapter<ToDoItemsRecyclerViewAdapter.ToDoItemViewHolder> {
    // Lista de TODOs
    private List<ToDoItem> todos;
    // base de datos de firebase
    private FirebaseFirestore db;
    private String userId;
    private Context context;

    /**
     * Constructor
     */
    public ToDoItemsRecyclerViewAdapter
            (List<ToDoItem> todos, FirebaseFirestore db, String userId) {
        this.todos = todos;
        this.db = db;
        this.userId = userId;
    }

    /**
     * Método para inflar nuestras celdas
     */
    @Override
    public ToDoItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewGroup view = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.todo_view, parent, false);
        context = parent.getContext();
        return new ToDoItemViewHolder(view);
    }

    /**
     * Método para configurar cada celda de acuerdo a su posicón con la información de nuestras todos
     */
    @Override
    public void onBindViewHolder(@NonNull ToDoItemViewHolder holder, final int position) {
        // seteamos todas las labels
        holder.title.setText(todos.get(position).getTitle());
        holder.description.setText(todos.get(position).getDescription());
        holder.completed.setChecked(todos.get(position).isCompleted());

        // Para las todos con localización abrimos google map si le da click
        if (todos.get(position).getLat() == null || todos.get(position).getLat().isEmpty()) {
            holder.marker.setVisibility(View.GONE);
        } else {
            holder.marker.setVisibility(View.VISIBLE);
            holder.marker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Creamos Intent para google maps y abrimos
                    String myLatitude = todos.get(position).getLat();
                    String myLongitude = todos.get(position).getLon();
                    String labelLocation = "Mi TODO";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:<" + myLatitude  + ">,<" + myLongitude + ">?q=<" + myLatitude  + ">,<" + myLongitude + ">(" + labelLocation + ")"));
                    context.startActivity(intent);
                }
            });
        }

        // seteamos el switch
        holder.completed.setChecked(todos.get(position).isCompleted());

        // Agregamos un listener para ver cuando cambia el switch
        holder.completed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            /**
             * En caso de que cambie el switch, debemos guardar en nuestra base de datos el nuevo valor
             * Así que obtenemos el objeto TODOs relacionado con este switch y se lo mandamos a la base de
             * datos de firebase con el id de la TODOs
             */
            @Override
            public void onCheckedChanged(final CompoundButton compoundButton, boolean b) {
                final ToDoItem todo = todos.get(position);
                todo.setCompleted(compoundButton.isChecked());

                // Actualizar el objeto en nuestra base de datos
                db.collection(userId).document(todo.getId()).set(todo)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                // Le mostramos al usuario si se actualizó correctamente o no
                                if (todo.isCompleted())
                                    Toast.makeText(context, "Todo completada", Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(context, "Todo incompleta", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
    }

    /**
     * Método para decirle al recyclerview cuantos objetos tenemos
     * @return
     */
    @Override
    public int getItemCount() {
        return todos.size();
    }

    /**
     * ViewHolder del recycler view con las propiedades que cada celda tendrá
     */
    class ToDoItemViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {
        final View mView;
        TextView title;
        TextView description;
        Switch completed;
        ImageView marker;

        // Constructor
        ToDoItemViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            title = mView.findViewById(R.id.tv_title);
            description = mView.findViewById(R.id.tv_description);
            completed = mView.findViewById(R.id.switch_done);
            marker = mView.findViewById(R.id.marker);

            // Click largo para borrar TODOs
            mView.setOnLongClickListener(this);
        }

        /**
         * Listener de clicks largos para borrar todos
         */
        @Override
        public boolean onLongClick(View view) {
            final int position = getAdapterPosition();
            // obtenemos el TODOs
            ToDoItem currentItem = todos.get(position);

            // lo borramos en nuestra db
            db.collection(userId).document(currentItem.getId()).delete()
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            // Una vez borrado de la base de datos, actualizamos el recycler view
                            todos.remove(position);
                            notifyDataSetChanged();

                            // Mostramos un mensaje al usuario de que se borro el TODOs
                            Toast.makeText(context, "Todo borrada", Toast.LENGTH_SHORT).show();
                        }
                    });
            return true;
        }
    }
}

