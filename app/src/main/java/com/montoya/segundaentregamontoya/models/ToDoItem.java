package com.montoya.segundaentregamontoya.models;

/**
 * Modelo de los TODOs junto con los métodos para obtener sus propiedades.
 * Usaremos este modelo para codificar y decodificar las respuestas y envios
 * a nuestra base de datos de firebase
 */
public class ToDoItem {

    // Propiedades
    private String id;
    private String title;
    private String description;
    private boolean completed;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    private String lat, lon;

    // Constructor
    public ToDoItem(String id, String title, String description, Boolean completed) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.completed = completed;
    }

    // Constructor
    public ToDoItem(String id, String title, String description, Boolean completed, String lat, String lon) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.completed = completed;
        this.lat = lat;
        this.lon = lon;
    }

    // Métodos para obtener y setear propiedades

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
