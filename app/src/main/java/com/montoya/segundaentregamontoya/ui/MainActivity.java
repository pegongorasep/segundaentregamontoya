package com.montoya.segundaentregamontoya.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.HandlerThread;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.montoya.segundaentregamontoya.R;
import com.montoya.segundaentregamontoya.adapters.ToDoItemsRecyclerViewAdapter;
import com.montoya.segundaentregamontoya.databinding.ActivityMainBinding;
import com.montoya.segundaentregamontoya.models.ToDoItem;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Actividad principal que se carga solo para los usuarios logeados
 *
 * Aquí vamos a cargar en un recycler view todos los TODOs que el usuario tenga
 * Además vamos a tener la capacidad de crear nuevos TODOs, de marcarlos como terminados y de borrarlos,
 * ademàs podremos crear TODOs con ubicación
 *
 * Desde aquí tambien vamos a mostrar en el menú la opción de ayuda con instrucciones para el usuario y la opción de perfil
 * para ver el perfil del usuario
 */
public class MainActivity extends AppCompatActivity {

    // Definición de variables
    private static final int REQUEST_CHECK_SETTINGS = 1;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 2;
    private String userId;
    List<ToDoItem> todos = new ArrayList<>();
    FirebaseFirestore db;

    // Variables de User Interface
    RecyclerView recyclerView;
    ToDoItemsRecyclerViewAdapter adapter;
    EditText title, description;
    FloatingActionButton fab, fabLocation;
    ProgressBar progressBar;

    // Variables para ubicación
    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    private Boolean requestingLocation = false;

    // Al salir de la actividad nos aseguramos de detener las peticiones de ubicación
    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    // Método para detener las peticiones de ubicación
    private void stopLocationUpdates() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                requestingLocation = false;
                progressBar.setVisibility(View.GONE);
                fusedLocationClient.removeLocationUpdates(locationCallback);
            }
        });
    }

    /**
     * Método OnCreate dónde obtenremos el usuario logeado, iniciaremos nuestros objetos de la ui y cargaremos los TODOs
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Para estar aquí tenemos que estar logeados, sino nos salimos de la actividad
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {

            // Inicializamos el fusedLocation client para las localizaciones
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    if (locationResult == null) {
                        return;
                    }
                    Toast.makeText(MainActivity.this, "location", Toast.LENGTH_SHORT).show();
                    for (Location location : locationResult.getLocations()) {
                        if (location.getAccuracy() < 50) {
                            stopLocationUpdates();

                            addToDo(title.getText().toString(), description.getText().toString(), location);
                        }
                    }
                }
            };

            // guardamos el id de usuario
            userId = user.getUid();

            // Binding para hacer referencia de nuestros objetos del XML
            ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
            View view = binding.getRoot();
            setContentView(view);

            // Obtenemos instancia de base de datos
            db = FirebaseFirestore.getInstance();

            // Inicializamos objetos de UI
            recyclerView = binding.todos;
            fab = binding.floatingActionButton;
            fabLocation = binding.fabTodoLocation;
            title = binding.etTitle;
            description = binding.etDescription;
            progressBar = binding.progressBar2;
            progressBar.setVisibility(View.GONE);

            // Inicializamos recyclerview con nuestro adapter
            adapter = new ToDoItemsRecyclerViewAdapter(todos, db, userId);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            // Inicializamos botón para agregar nuevos ToDos
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addToDo(title.getText().toString(), description.getText().toString());
                }
            });

            // Inicializamos botón para agregar nuevas TODOs con ubicación
            fabLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // Si ya estamos pidiendo la localización la cancelamos, por si el usuario se arrepiente
                    if (requestingLocation) {
                        stopLocationUpdates();


                    } else {
                        // Vemos que el usuario tenga su ubicación prendida para el tipo de localizaciones
                        // que vamos a necesitar, sino le pedimos que la prenda

                        // Construimos la petición de localización
                        final LocationRequest locationRequest = LocationRequest.create();
                        locationRequest.setInterval(1000);
                        locationRequest.setFastestInterval(1000);
                        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

                        // Checamos si esta prendida la ubicación
                        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                                .addLocationRequest(locationRequest);
                        SettingsClient client = LocationServices.getSettingsClient(MainActivity.this);
                        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

                        // En caso de que si seguimos
                        task.addOnSuccessListener(MainActivity.this, new OnSuccessListener<LocationSettingsResponse>() {
                            @Override
                            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

                                // Para android > 22 pedimos permiso para acceder a la ubicación
                                if (android.os.Build.VERSION.SDK_INT >= 23) {
                                    if (MainActivity.this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                                            || MainActivity.this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                                        ActivityCompat.requestPermissions(MainActivity.this,
                                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                                MY_PERMISSIONS_REQUEST_LOCATION);
                                        return;
                                    }
                                }

                                // Obtenemos las localizaciones en el background con un handler
                                final HandlerThread handlerThread = new HandlerThread("RequestLocation");
                                handlerThread.start();

                                requestingLocation = true;
                                progressBar.setVisibility(View.VISIBLE);
                                fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, handlerThread.getLooper());
                            }
                        });

                        // En caso de no tener ubicación encendida mandamos a settings
                        task.addOnFailureListener(MainActivity.this, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                if (e instanceof ResolvableApiException) {
                                    try {
                                        ResolvableApiException resolvable = (ResolvableApiException) e;
                                        resolvable.startResolutionForResult(MainActivity.this,
                                                REQUEST_CHECK_SETTINGS);
                                    } catch (IntentSender.SendIntentException ignored) {}
                                }
                            }
                        });
                    }
                }
            });

            // Obtenemos lista de todos
            loadToDoList();


        // Sino estamos logeados nos salimos
        } else {
            finish();
        }
    }

    /**
     * Método OnResume dónde revisaremos que el usuario este logeado y sino lo esta lo mandaremos a la actividad de login
     */
    @Override
    protected void onResume() {
        super.onResume();

        // Para estar aquí tenemos que estar logeados, sino nos salimos de la actividad
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            finish();
        }
    }

    private void addToDo(String titleText, String descriptionText) {
        addToDo(titleText, descriptionText, null);
    }

    /**
     * Método para añadir nuevas Todos
     */
    private void addToDo(String titleText, String descriptionText, Location location) {
        // Sólo revisamos que no esten vacios los textos para no hacer todos en blanco
        if (titleText != null && !titleText.isEmpty()) {

            ToDoItem todo;

            if (location != null) {
                // Creamos una nueva ToDoItem con ubicación
                todo = new ToDoItem(UUID.randomUUID().toString(), titleText, descriptionText, false,
                        String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
            } else {
                // Creamos una nueva ToDoItem
                todo = new ToDoItem(UUID.randomUUID().toString(), titleText, descriptionText, false);
            }

            // Lo mandamos guardar en la base de datos de Firestore dentro de la base de datos de éste usuario particular
            db.collection(userId).document(todo.getId()).set(todo).addOnCompleteListener(new OnCompleteListener<Void>() {

                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    title.setText("");
                    description.setText("");

                    // Refrescamos la lista de todos
                    loadToDoList();
                    // Avisamos al usuario que se agrego correctamente
                    Toast.makeText(MainActivity.this, "Todo agregada!", Toast.LENGTH_LONG).show();
                }
            });

        // No se pueden crear todos en blanco
        } else {
            Toast.makeText(MainActivity.this, "No se pueden crear todos en blanco", Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Método para obtener todas nuestras todos desde nuestra base de datos en Firestore
     */
    private void loadToDoList() {
        progressBar.setVisibility(View.VISIBLE);
        // Borramos todas las todos para obtener la nuevas
        todos.clear();

        // Obtenemos todos
        db.collection(userId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    // Para cada objeto de nuestra db creamos una ToDoItem
                    for (DocumentSnapshot doc: task.getResult()) {

                        // Todos
                        ToDoItem todo = new ToDoItem(
                                doc.getString("id"),
                                doc.getString("title"),
                                doc.getString("description"),
                                doc.getBoolean("completed"),
                                doc.getString("lat"),
                                doc.getString("lon"));

                        // Agregar a nuestra lista cada una
                        todos.add(todo);
                    }
                    // Notificar al adapter para que el recycler view se actualice
                    adapter.notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);
                }
        });
    }


    /**
     * Con este método creamos el menú en el toolbar con el archivo de menu_main
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Método llamado al hacer click en algun item del menú
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // Si da click en la opción perfil abrimos la actividad de perfil
        if (id == R.id.action_profile) {
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
            return true;

        // Si da click en ayuda desplegamos las instrucciones
        } else
            if (id == R.id.action_help) {
                new AlertDialog.Builder(this)
                        .setTitle("Instrucciones")
                        .setMessage("Manten presionada un TODO para eliminarlo" +
                                "\n\nClick en el switch para marcarlo como completado" +
                                "\n\nClick en el ícono de localización azul para abrir la úbicación de un TODO" +
                                "\n\nClick en el botón '+' para agregar TODOs" +
                                "\n\nClick en el botón arriba de '+' para agregar TODOs con localización")

                        .setPositiveButton(android.R.string.yes, null).show();

                return true;
            }
        return super.onOptionsItemSelected(item);
    }
}
